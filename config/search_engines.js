var ENGINES={
	list:[],
	index:{},
	short_index:{},
	enabled:[],
	all_engines_pattern_of_search_text:undefined,
	engines_pattern_of_search_text:undefined
}

ENGINES.list.push(
	{
		name:"baidu",
		short_name:"b",
		icon:"./resources/icon/baidu.png",
		search_url:"https://www.baidu.com/s?wd=",
		keyword_parser:function(json){
			if(json.g){
				return json.g.map(i=>{return i.q})
			}else{
				return []
			}
		},
		keyword_request_body_creater:function(searchText){
			return {
				url:"https://www.baidu.com/sugrec?ie=utf-8&json=1&prod=pc&from=pc_web",
				type:"GET",
				timeout: 5000,
				dataType: 'jsonp',
				data:{
					["wd"]:searchText
				},
			}
		}
	}
)

ENGINES.list.push(
	{
		name:"bing",
		short_name:"i",
		icon:"./resources/icon/bing.png",
		search_url:"https://cn.bing.com/search?q=",
		keyword_parser:function(json){
			if(json.AS&&json.AS.Results&&json.AS.Results[0]&&json.AS.Results[0].Suggests){
				return json.AS.Results[0].Suggests.map(i=>{return i.Txt})
			}else{
				return []
			}
		},
		keyword_request_body_creater:function(searchText){
			return {
				url:"http://api.bing.com/qsonhs.aspx?type=cb&cb=?",
				type:"GET",
				timeout: 5000,
				dataType: 'jsonp',
				data:{
					["q"]:searchText
				},
			}
		}
	}
)

ENGINES.list.push(
	{
		name:"google",
		short_name:"g",
		icon:"./resources/icon/google.png",
		search_url:"https://www.google.com/search?q=",
		keyword_parser:function(json){
			return json[1].map(i=>{return i[0]})
		},
		keyword_request_body_creater:function(searchText){
			return {
				url:"http://suggestqueries.google.com/complete/search?client=psy-ab&jsonp=?",
				type:"GET",
				timeout: 5000,
				dataType: 'jsonp',
				data:{
					["q"]:searchText
				},
			}
		}
	}
)

//如果不需要后面两个搜索引擎，可以把它们删掉即可，
//这两个搜索引擎没有开放关键词的跨域访问api，所以需要用bin/keyword_server.py作为代理

ENGINES.list.push(
	{
		name:"dogedoge",
		short_name:"d",
		icon:"./resources/icon/dogedoge.png",
		search_url:"https://www.dogedoge.com/results?q=",
		keyword_parser:function(json){
			return json
		},
		keyword_request_body_creater:function(searchText){
			return {
				url:"https://www.dogedoge.com/",
				type:"GET",
				timeout: 5000,
				dataType: 'json',
				data:{
					["q"]:searchText
				},
			}
		}
	}
)

ENGINES.list.push(
	{
		name:"zhihu",
		short_name:"z",
		icon:"./resources/icon/zhihu.png",
		search_url:"https://www.zhihu.com/search?q=",
		keyword_parser:function(json){
			return json
		},
		keyword_request_body_creater:function(searchText){
			return {
				url:"http://127.0.0.1:1554/zhihu?",
				type:"GET",
				timeout: 5000,
				dataType: 'json',
				data:{
					["q"]:searchText
				},
			}
		}
	}
)

for(var _e of ENGINES.list){
	ENGINES.index[_e.name]=_e
	ENGINES.short_index[_e.short_name]=_e
}

ENGINES.all_engines_pattern_of_search_text=/:A $/g
ENGINES.engines_pattern_of_search_text=eval("/:(" + Object.keys(ENGINES.short_index).join("|") + ")+ $/g")
ENGINES.searchText=undefined