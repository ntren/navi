import json
from html.parser import HTMLParser
from http.server import BaseHTTPRequestHandler, HTTPServer
from urllib import parse
import requests


##################################################################################
# Doge Doge
##################################################################################
class DogeDogeKWParser(HTMLParser):
    def __init__(self):
        super().__init__()
        self.kw_list = []
        self.kw_buffer = ""

    def handle_endtag(self, tag):
        if tag == "div":
            self.kw_list.append(self.kw_buffer)
            self.kw_buffer = ""

    def handle_data(self, data):
        self.kw_buffer += data


dogedoge_headers = {
    "user-agent": """Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36"""
}


def query_dogedoge_kw(search_text):
    url = f"https://www.dogedoge.com/sugg/{search_text}"
    respond = requests.get(url=url, timeout=2, headers=dogedoge_headers)
    status_code, text = respond.status_code, respond.text
    if status_code == 200:
        html_parser = DogeDogeKWParser()
        html_parser.feed(text)
        return status_code, html_parser.kw_list
    else:
        return status_code, []


##################################################################################
# 知乎
##################################################################################
zhihu_headers = {
    "user-agent": """Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36"""
}


def query_zhihu_kw(search_text):
    url = f"https://www.zhihu.com/api/v4/search/suggest?"
    respond = requests.get(url=url, params={'q': search_text}, timeout=2, headers=zhihu_headers)
    status_code, text = respond.status_code, respond.text
    if status_code == 200:
        return status_code, [item['query'] for item in json.loads(text)['suggest']]
    else:
        return status_code, []


##################################################################################
# 服务
##################################################################################
kw_query_fun_dict = {
    "/dogedoge": query_dogedoge_kw,
    "/zhihu": query_zhihu_kw
}


class RequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        parsed_url = parse.urlparse(self.path)
        kw_query_fun = kw_query_fun_dict[parsed_url.path]
        params = parse.parse_qs(parsed_url.query)
        search_text = params["q"][0]
        status_code, kw_list = kw_query_fun(search_text)
        self._write_result_(status=status_code, kw_list=kw_list)

    def _write_result_(self, status=200, kw_list=[]):
        kw_dumped = json.dumps(kw_list).encode("utf-8")
        self.send_response(status)
        self.send_header("Content-type", "application/json")
        self.send_header("Content-Length", str(len(kw_dumped)))
        self.send_header("Access-Control-Allow-Origin", "*")
        self.end_headers()
        self.wfile.write(kw_dumped)


if __name__ == '__main__':
    server = HTTPServer(("127.0.0.1", 1554), RequestHandler)
    server.serve_forever()
